ENGINE = lualatex
FILENAME = base.report.tex
CODE = $(shell grep -o -P '(?<=code\{).*(?=\})' $(FILENAME))
POSTNAME =
#POSTNAME = -ZIzaola



ifeq ($(CODE), )
	JOBNAME = base.report
else
	JOBNAME = $(CODE)
endif
FINALNAME = $(JOBNAME)$(POSTNAME)

wbs: 
	$(MAKE) -C $@ $(MAKECMDGOALS)

all:
	make wbs
	make compile
#	make compressimage
	mv base.report.pdf informe_final.pdf

compile: $(FILENAME) businesscase.tex dataanalysis.tex risks.tex 
#	$(ENGINE) -shell-escape -jobname $(FINALNAME) $(FILENAME)
#	$(ENGINE) -shell-escape -jobname $(FINALNAME) $(FILENAME)
	$(ENGINE) -shell-escape $(FILENAME)
	biber $(JOBNAME)
	$(ENGINE) -shell-escape $(FINALNAME) $(FILENAME)
	$(ENGINE) -shell-escape $(FINALNAME) $(FILENAME)


presentation: base.presentation.tex
	$(ENGINE) -shell-escape base.presentation.tex
	$(ENGINE) -shell-escape base.presentation.tex
	$(ENGINE) -shell-escape base.presentation.tex
	mv base.presentation.pdf presentation.pdf

# To reduce the filesize by reducing the resolution of included bitmaps.
# http://tex.stackexchange.com/questions/14429/pdftex-reduce-pdf-size-reduce-image-quality
compressimage:
	gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/prepress -dNOPAUSE -dQUIET -dBATCH -sOutputFile=$(FINALNAME)-reduced.pdf $(FINALNAME).pdf
	mv $(FINALNAME)-reduced.pdf   $(FINALNAME).pdf


risks.tex: Risk_Register.xlsx extract_risk_table.py pretify_risks_table.awk
	python3 extract_risk_table.py
	soffice --headless --convert-to ods risks.xlsx
	ssconvert  risks.ods risks_tmp.tex
	iconv -f iso-8859-1 -t utf8 risks_tmp.tex -o risks_p_tmp.tex
	./pretify_risks_table.awk risks_p_tmp.tex > risks.tex
	rm risks_tmp.tex
	rm risks_p_tmp.tex

actions.tex: Risk_Register.xlsx extract_action_table.py pretify_risks_table.awk
	python3 extract_action_table.py
	soffice --headless --convert-to ods actions.xlsx
	ssconvert  actions.ods actions_tmp.tex
	iconv -f iso-8859-1 -t utf8 actions_tmp.tex -o actions_p_tmp.tex
	./pretify_risks_table.awk actions_p_tmp.tex > actions.tex
	rm actions_tmp.tex
	rm actions_p_tmp.tex




clean:
	rm $(JOBNAME).pdf *.aux *.log *.toc *.out *.bbl *.blg *.bcf *.run.xml *.hst *.ver *.sta

