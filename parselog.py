#!/usr/bin/env python2

import subprocess
import re
import sys

import cStringIO, Texml.texmlwr

def make_initials(name):
  name, surname = name.split()
  return '%s. %s' %(name[0], surname)

# Code from http://uucode.com/blog/2008/10/17/escape-a-tex-string-in-python/
# used to escape latex strings
class TexEscape:
  def __init__(self):
    self.stream = cStringIO.StringIO()
    self.texmlwr = Texml.texmlwr.texmlwr(self.stream, 'UTF-8', '72')
  def escape(self, s):
    self.texmlwr.write("x\n")
    self.stream.truncate(0)
    self.texmlwr.write(s)
    return self.stream.getvalue()
  def free(self):
    self.stream.close()

p = re.compile(r"""
(?P<revno>\w+)               # revno number
:\s*                         # separtor
(?P<name>.*)                 # Name of comitter
\s
(?P<year>\d\d\d\d)          
-
(?P<month>\d\d)             
-
(?P<day>\d\d)
\s+
(?P<tag>:.*:)?       # optional tag for revno
\s*
(?P<log>.*)                # log message
""",re.VERBOSE)


ptag = re.compile(r"""
\s*
\(
(?P<commit>.*,\s*)?
(?P<tag>tag: .*?)
(?P<branch>, .*)?
\)
""",re.VERBOSE)

def parse(line):
    m =  p.search(line)
    return m

texstring = TexEscape()    
oldtag = None

#f=os.popen("bzr log --line")# --forward")
command = 'git log --date=short --pretty=format:"%h: %an %ad :%d: %s" --encoding="UTF-8"'
if len(sys.argv) == 1:
  command = command + ' | grep tag:'
f = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE).stdout
print "\\begin{versionhistory}"
for line in f.readlines():
     match = parse(line)
     tag = match.group("tag")
     if tag != None:
         tag = tag[1:-1]
         oldtag = tag
     else:
         tag = oldtag
     #print "{%s}" %(tag)
     #parse the tag part
     fulltag = ptag.match(tag)
     newtag =""
     if fulltag:
       #print "Process"
       #print "groups"
       #print fulltag.group("tag")
       #print fulltag.group("commit")
       #print fulltag.group("branch")
       tag = fulltag.group("tag")
       tag = tag.split(":")[1]
     else:
       tag = ""
     #print tag
     tag = tag.strip()
     name = make_initials( match.group("name"))
     log = match.group("log")
     year = match.group("year")
     month = match.group("month")
     day = match.group("day")
     # escape the log text
     #log = texstring.escape(log)
     print "\\vhEntry{%s}{%s-%s-%s}{%s}{%s}" % (tag, year, month, day, name, log)
print "\\end{versionhistory}"

texstring.free()
