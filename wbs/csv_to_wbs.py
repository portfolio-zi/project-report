import os
import csv
import subprocess
import jinja2
#from pdflatex import PDFLaTeX

latex_jinja_env = jinja2.Environment(
    block_start_string='\BLOCK{',
    block_end_string='}',
    variable_start_string='\VAR{',
    variable_end_string='}',
    comment_start_string='\#{',
    comment_end_string='}',
    line_statement_prefix='%-',
    line_comment_prefix='%#',
    trim_blocks=True,
    autoescape=False,
    loader=jinja2.FileSystemLoader(os.path.abspath('.'))
)

# read data
data = {}
tasks = []
with open('project.csv') as csv_file:
    where = 'task'
    csv_reader = csv.reader(csv_file, delimiter=',')
    for row in csv_reader:

        if row[0].startswith('WP'):
            if len(tasks) != 0:
                data[wp_label] = tasks
                tasks = []
            wp_label = "{} {}".format(row[0], row[1])
            where = 'wp'
            print(wp_label)
        elif row[0].startswith('T'):
            label = "{} {}".format(row[0], row[1])
            where = 'task'
            print(label)
            tasks.append(label)
        else:
            raise ValueError
    data[wp_label] = tasks  # for the last WP


# print(data)

template = latex_jinja_env.get_template('wbs_template.tex')
document = template.render(wbs=data)
with open('wbs.tex', 'w') as output:
    output.write(document)
#    os.system('pdflatex "wbs.tex"')
#    pdfl = PDFLaTeX.from_texfile('wbs.tex')
#    pdf, log, completed_process = pdfl.create_pdf(
#        keep_pdf_file=True, keep_log_file=True)
#    if completed_process.retruncode != 0:
#        print('Exit-code not 0 for ' + 'wbs.tex ', 'check Code!')
