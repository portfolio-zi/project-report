from collections import OrderedDict
import os
import csv
import subprocess
import jinja2

from mapro.tasks import Task
from mapro.duration import TaskDuration
from mapro.project import WorkPackage

latex_jinja_env = jinja2.Environment(
    block_start_string='\BLOCK{',
    block_end_string='}',
    variable_start_string='\VAR{',
    variable_end_string='}',
    comment_start_string='\#{',
    comment_end_string='}',
    line_statement_prefix='%-',
    line_comment_prefix='%#',
    trim_blocks=True,
    autoescape=False,
    loader=jinja2.FileSystemLoader(os.path.abspath('.'))
)


# read data
data = OrderedDict()
tasks = []
with open('project.csv') as csv_file:
    where = 'task'
    csv_reader = csv.reader(csv_file, delimiter=',')
    for row in csv_reader:

        if row[0].startswith('WP'):
            if len(tasks) != 0:
                print(type(tasks))
                wp.tasks = tasks
                data[wp] = tasks
                tasks = []
            wp_id = row[0]
            wp = WorkPackage(row[0], row[1])
            where = 'wp'
        elif row[0].startswith('T'):
            task_id = row[0]
            task_name = row[1]
            next = row[2]
            if next:
                next = next.split(':')
                next_id, next_type = next[0], next[1]
                next_type = next_type.lower()
                next_type = next_type[0] + '-' + next_type[-1]
                next_task = (next_id, next_type)
            else:
                next_task = None
            duration = float(row[3])
            estimator1 = float(row[4])
            estimator2 = float(row[5])
            task_duration = TaskDuration(
                duration, 'pert', None, (estimator1, estimator2))
            start_date = None
            task = Task(task_id, task_name, task_duration,
                        start_date, next_task)
            where = 'task'
            tasks.append(task)
        else:
            raise ValueError
    print(type(tasks))
    wp.tasks = tasks
    data[wp] = tasks  # for the last WP


# print(data)

all_tasks = []
for wp in data.keys():
    tasks = data[wp]
    for task in tasks:
        all_tasks.append(task)

previous_task = None
for task in all_tasks:
    task.calculate_start_date(previous_task)
    task.calculate_end_date()
    previous_task = task
    print(task.start_date, task.end_date)

for wp in data.keys():
    wp.calculate_start_end_dates()
    print(wp.start_date, wp.end_date)

template = latex_jinja_env.get_template('gantt_template.tex')
document = template.render(project=data)
with open('gantt.tex', 'w') as output:
    output.write(document)

#    os.system('pdflatex "gantt.tex"')
#    pdfl = PDFLaTeX.from_texfile('gantt.tex')
#    pdf, log, completed_process = pdfl.create_pdf(
#        keep_pdf_file=True, keep_log_file=True)
#    if completed_process.retruncode != 0:
#        print('Exit-code not 0 for ' + 'gantt.tex ', 'check Code!')
