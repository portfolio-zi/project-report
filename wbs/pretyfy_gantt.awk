#!/usr/bin/awk -f
/WP.0/{print "\
\\ganttgroup[group label font=\\color{fase1}\\bfseries, group/.append style={draw=white, fill=white}]{Fase Inicial}{2020-01-02}{2020-06-27} \\\\"}
/Definir alcance/{print "\
\\ganttset{group/.append style={fill=fase2}}\
\\ganttset{bar/.append style={draw=black, fill=fase2}}\
\n\
    \\ganttgroup[group label font=\\color{fase2}\\bfseries, group/.append style={draw=white, fill=white}]{Fase Organización}{2020-01-02}{2020-06-27} \\\\"}
/WP.1/{print "\
\\ganttset{group/.append style={fill=fase3}}\\ganttset{bar/.append style={draw=black, fill=fase3}}\
\n\
\\ganttgroup[group label font=\\color{fase3}\\bfseries,group/.append style={draw=white, fill=white}]{Fase Ejecución}{2020-01-02}{2020-06-27} \\\\"}
/WP.4/{print "\
\\ganttset{group/.append style={fill=fase4}}\\ganttset{bar/.append style={draw=black, fill=fase4}}\
\n\
\\ganttgroup[group label font=\\color{fase4}\\bfseries,group/.append style={draw=white, fill=white}]{Fase Cierre}{2020-01-02}{2020-06-27} \\\\"}
1
