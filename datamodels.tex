\mode<article>
\chapter{Generación, aplicación y evaluación de modelos de \inenglish{machine learning}}
\label{sec:gener-aplic-y}
\mode*


% Notebook:
% • Tareas técnicas de selección de algoritmos de machine learning, preparación de los
% datos como input para los algoritmos, estrategias de segmentación de los datos para
% las tareas de training/validation/test de modelos, parametrización de los algoritmos
% para la generación de diferentes alternativas de modelos, evaluación del rendimiento
% de los modelos obtenidos y selección final.
% Contenidos para este apartado en la memoria:
% • Describir y argumentar la estrategia empleada para planificar y estructurar las tareas
% de generación, aplicación y evaluación de modelos: selección de algoritmos,
% estrategias de segmentación de datos para el entrenamiento y de alternativas de
% parametrización de los algoritmos, métricas para la evaluación de los modelos.
% • Caracterizar los resultados del análisis, a través de las diferentes métricas de precisión
% y rendimiento utilizadas sobre los modelos generados.
% • Conclusiones acerca de la alineación de los modelos obtenidos y su rendimiento con
% los requerimientos del problema a resolver (¿Vemos factible cumplir los requisitos del
% problema planteado con los modelos aprendidos? ¿Hemos respondido a la motivación
% por la que iniciamos el proyecto? ¿En qué medida nos hemos desviado y por qué?).

\mode<presentation>

\begin{frame}
  \frametitle{Modelo predictivo}
  \begin{block}{Distribución de datos}
    \begin{itemize}
      \item 1 semana para validación
      \item \SI{70}{\percent} \emph{train} --- \SI{30}{\percent} test
%      \item Métrica de comparación: \ac{mape}
    \end{itemize}
  \end{block}
  \begin{block}{Dos modelos de aprendizaje}
    \begin{enumerate}
      \item \Acl{rf}
      \item \Acl{ann}
    \end{enumerate}
  \end{block}
  \begin{block}{Métrica}
    \Acl{mape}
  \end{block}
\end{frame}
\mode*

\section{Descripción de los modelos generados}
\label{sec:descripcion-de-los}

El problema planteado requiere predecir un único número, por lo que es necesario crear un sistema de regresión. Se ha planteado comparar dos tipos de aprendizaje supervisado: \ac{rf} y \ac{ann}.

Los \ac{rf} \cite{rf-sklearn, breiman2001} ensamblan las predicciones de múltiples árboles predictores. Son relativamente simples y no tienen muchos parámetros a determinar. El entrenamiento y la predicción con este tipo de modelos suele ser rápido.

Se ha optado por utilizar una \ac{ann} \cite{ann-keras, rumelhart1986} con dos capas ocultas densamente conectadas. Las \ac{ann} son modelos muy flexibles que pueden usarse en muchos problemas. Suelen tener muchos parámetros a ajustar, especialmente usando capas densamente conectadas, y por lo tanto, suelen requerir bastantes recursos de computación.

Se han generado dos modelos por cada estrategia de aprendizaje: uno con todas las \feature\ recogidas y otro con las características no eliminadas en el proceso descrito en \cref{sec:rfe}. 

\section{Estrategias de segmentación de datos y evaluación de modelos}

De los aproximadamente 42 días de datos que se disponen, se han reservado los datos de la última semana para validar los modelos y poder compararlos entre sí. Para el entrenamiento se ha escogido un procedimiento diferente para cada estrategia de aprendizaje. En los \ac{rf} se ha optado por usar \textquote[][]{cross validation} y un optimizador de hyperparámetros. En este caso se ha utilizado todo el conjunto de datos (menos los de validación) y se ha dividido en $3$ grupos. El optimizador \variable{Optuna} \cite{optuna2019} se encarga de generar el espacio de búsqueda, con la definición de los hyperparámetros a optimizar, y llevar a cabo las iteraciones para minimizar el criterio seleccionado. En el caso de la \ac{ann} se han dividido los datos para crear el modelo, en un conjunto de entrenamiento y otro de test en un ratio de 0.7/0.3. 

Estos procedimientos han estado supeditados a la capacidad de cálculo utilizado en este proyecto. Se ha utilizado un portátil con Intel Core i5 y \SI{8}{\gibi\byte} de \textsc{RAM}. Las 150 iteraciones realizadas con el optimizador han requerido de \SI[parse-numbers=false]{02:48}{\minute}, mientras que para entrenar una única \ac{ann} se ha necesitado \SI[parse-numbers=false]{01{:}51}{\minute}. 

El objetivo final es predecir el porcentaje de sílice con una precisión del \SI{10}{\percent}. Par ello, como criterio de evaluación de los modelos se ha utilizado la métrica \ac{mape}:
\begin{equation}
  M=\frac{1}{n}\sum_{t=1}^n\left|\frac{A_t-F_t}{A_t}\right|,
\end{equation}
donde $A_t$ es el valor real y $F_t$ es el valor predicho.

\section{Modelos basados en \acl{rf}}
\label{sec:modelos-basados-en-rf}
\epigraph{(Los cálculos presentados en esta sección se encuentran en el notebook CRISP-DM\_05\_rf.ipynb)}{}

La implementación de \ac{rf} utilizado en este proyecto es la de la librería \variable{scikit-learn}. El número de estimadores (límites $2$ y $32$) y la profundidad máxima (límites $20$ y $200$, en pasos de $20$) han sido optimizado por Optuna en un ciclo de $150$ iteraciones. El optimizador ha minimizado el \inenglish{scoring} \ac{mae}\footnote{scikit-learn no proporciona la métrica \acl{mape}, al parecer, porque puede crear problemas de divisiones por $0$.} mediante un \inenglish{cross-validation} de $3$ grupos. 


Para el modelo con todas las \feature, el valor mínimo obtenido ha sido $0.8907$ con una profundidad máxima de $3$ y número de estimadores de $14$. Los datos de validación han mostrado un \ac{mae} de $1.04$, un \SI{17}{\percent} mayor al valor obtenido en el entrenamiento. El \ac{mape} calculado es de \SI{40.8}{\percent} (ver \cref{fig:rf_full}). 

Sin embargo, para el modelo con menos \feature, el valor mínimo obtenido ha sido $0.8892$ con una profundidad máxima de $2$ y número de estimadores de $40$. Los datos de validación han mostrado un \ac{mae} de $0.63$, un \SI{30}{\percent} menor al valor obtenido en el entrenamiento. El \ac{mape} calculado es de \SI{26.3}{\percent} (ver \cref{fig:rf_reduced}). 

\begin{figure}
  \centering
  \begin{subfigure}[b]{0.3\textwidth}
        \includegraphics[width=\textwidth]{../data/rf-full-validation.pdf}
        \caption{\acs{rf} con todas las características}
        \label{fig:rf_full}
    \end{subfigure}\quad
  \begin{subfigure}[b]{0.3\textwidth}
        \includegraphics[width=\textwidth]{../data/rf-reduced-validation.pdf}
        \caption{\acs{rf} con características reducidas}
        \label{fig:rf_reduced}
    \end{subfigure}\quad
  \begin{subfigure}[b]{0.3\textwidth}
        \includegraphics[width=\textwidth]{../data/ann-full-validation.pdf}
        \caption{\acs{ann} con todas las características}
        \label{fig:ann_full}
    \end{subfigure}
  \caption{Visualización de la predicción y datos reales de validación.}
  \label{fig:validacion}
\end{figure}


\mode<presentation>
\begin{frame}
  \frametitle{\Acl{rf}}
    \begin{itemize}
    \item Optimización de hyperparámetros (Optuna)
    \item Estimadores: 2--32
    \item Profundidad: 20--200 (paso 20)
    \item Métrica: \acl{mae}
    \item \emph{Cross Validation}: 3 grupos
    \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{\Acl{rf} - Resultados}
  \only<1>{%
    \centering
    \begin{tabular}{@{}lll@{}}
      \toprule
      & Todas & Reducidas \\
      \midrule
      Estimadores & 14  & 40\\
      Profundidad & 3 & 2\\
      \ac{mae}    & 1.04 & 0.63 \\
      \ac{mape}   & \SI{40.8}{\percent} & \SI{26.3}{\percent}\\
      \bottomrule
    \end{tabular}
  }
  \only<2>{
    \centering
    \includegraphics[width=\textwidth]{../data/rf-full-validation.pdf}
    }
  \only<3>{
    \includegraphics[width=\textwidth]{../data/rf-reduced-validation.pdf}
    }
\end{frame}
\mode*

\section{Modelos basados en \acl{ann}}
\label{sec:modelos-basados-en-ann}
\epigraph{(Los cálculos presentados en esta sección se encuentran en el notebook CRISP-DM\_05\_neural.ipynb)}{}

Se ha utilizado la librería \variable{keras} para modelar la \acl{ann}. Se ha generado una red con $4$ capas. La capa de entrada cuenta $21$ nodos, el número de características. La capa oculta se ha creado con $1.5$ veces el número de nodos iniciales; es decir, $32$ nodos\footnote{Redondeando a número entero.}. La tercera capa cuenta otra vez con un número de nodos igual al de características. Estás $3$ capas son densas en el sentido que unen todos los nodos de una capa con todos los nodos de la capa siguiente. 
Finalmente, la capa de salida, activada con una función lineal, al ser un problema de regresión de un único parámetro, cuenta con un único nodo. Se ha utilizado la función $\ReLU(x)$ (ver \cref{sec:relu}) para activar las primeras $3$ capas\footnote{Al utilizar $\ReLU$ en la capa final la métrica \ac{mape} no ha bajado de \SI{100}{\percent}, lo que lleva a pensar que los valores de entrada en el nodo de la última capa son negativos.}.

El modelo ha sido compilado con el optimizador \variable{adam} \cite{adam} y $150$ \textquote[][]{épocas}.
El criterio de minimización ha sido \ac{mape}\footnote{La librería \variable{keras}, a diferencia de \variable{scikit-learn}, sí implementa la métrica \ac{mape}.}.
La red ha sido entrenada con el \SI{70}{\percent} de los datos. 

El valor final de \ac{mape} obtenido ha sido de $15.936$, oscilando entre este valor y $15.959$ en las últimas $6$ iteraciones. Los datos de test han arrojado un valor \ac{mape} de $16.12$. Con este modelo no se ha hecho ningún trabajo de \inenglish{cross-validation,} ni se ha creado un modelo para el conjunto de datos reducido por falta de tiempo. Las predicciones sobre los datos de validación han mostrado un error mucho mayor, con un \ac{mape} de \SI{45.49}{\percent} (ver \cref{fig:ann_full}). 


\section{Discusión de los resultados}
\label{sec:discusion-de-los}

En cuanto a los modelos basados en \ac{rf}, los resultados han sido decepcionantes. En el caso del conjunto completo de datos, a pesar de la validación cruzada el error ha sido más del doble en los datos de validación. Esto puede indicar que el modelo ha sido sobre entrenado para el conjunto de datos iniciales.
Para los datos reducidos se ha obtenido un error (\ac{mae}) menor en validación que en entrenamiento. Es un resultado para el que el autor no encuentra explicación.
En el modelo \ac{ann} el error (\ac{mape}) de test es ligeramente mayor que el de entrenamiento, como es esperable. Sin embargo, con valores de validación el \ac{mape} se dispara más del doble. 

En todo caso, los modelos están lejos de cumplir el criterio de éxito de predecir el contenido de sílice con un error inferior al \SI{10}{\percent}. Alcanzar este objetivo requerirá mayor trabajo sobre los datos.


\mode<presentation>
\begin{frame}
  \frametitle{\acl{ann}}
  \begin{itemize}
  \item Librería Keras
  \item 4 capas densamente conectadas
  \item Regresión: última capa 1 nodo
  \item Nodos: $N_f:1.5N_f:N_f:1$
  \item Métrica: \ac{mape}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{\acl{ann} - Resultados}
  \only<1>{%
    \centering
  \begin{tabular}{@{}ll@{}}
    \toprule
    & \ac{mape} \\
    \midrule
    \emph{train} & $15.936$\\
    \emph{test}  & $16.12$\\
    \emph{Valiation} & $45.49$\\
    \bottomrule
  \end{tabular}
}
\only<2>{%
  \includegraphics[width=\textwidth]{../data/ann-full-validation.pdf}  
  }
\end{frame}

\mode*


\section{Líneas futuras de trabajo}
\label{sec:lineas-futuras-de}

Entre las lineas futuras de trabajo podemos destacar las siguientes:
\begin{itemize}
\item Estudiar si normalizar los datos de entrada de los modelos \ac{rf} mejora la predicción.
\item Estudiar el efecto de otros hyperparámetros.
\item Normalizar los \inenglish{targets} en los modelos \ac{ann} para poder utilizar un función de activación de tipo \textquote[][]{sigmoid}.
\item Una vez obtenido un error razonable, implementar los modelos para el valor del sílice desplazado dos horas.
\end{itemize}






%%% Local Variables:
%%% TeX-parse-self: t
%%% TeX-auto-save: t
%%% TeX-master: "base.report"
%%% TeX-engine: luatex
%%% TeX-command-extra-options: "-shell-escape"
%%% ispell-local-dictionary: "castellano"
%%% End:
