\NeedsTeXFormat{LaTeX2e}[1999/12/01]
\ProvidesPackage{crispdm}[2019/11/16 v0.1]

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{scrreport}}
\PassOptionsToClass{a4paper,12pt, titlepage=true, openright}{scrreport}
\ProcessOptions\relax

\LoadClassWithOptions{scrreport}

\renewcommand*{\titlepagestyle}{empty}

%%%
\usepackage{beamerarticle}

%%% Font settings
\usepackage{amsmath}

\usepackage{crispdm-fonts}



\DeclareRobustCommand*{\version}[1]{%
\DeclareRobustCommand*{\theversion}{#1}%
}
\DeclareRobustCommand*{\code}[1]{%
\DeclareRobustCommand*{\thecode}{#1}%
}
\DeclareRobustCommand*{\project}[1]{%
\DeclareRobustCommand*{\theproject}{#1}%
}
\DeclareRobustCommand*{\revised}[1]{%
\DeclareRobustCommand*{\therevised}{#1}%
}
\DeclareRobustCommand*{\approved}[1]{%
\DeclareRobustCommand*{\theapproved}{#1}%
}


 
%%% Generate data for the revision table.

\usepackage{vhistory}

%%% Translate table
\def \vh@SpanishCaptions{%
  \def\vhhistoryname{Historia de revisiones}%
  \def\vhversionname{Revisión}%
  \def\vhdatename{Fecha}%
  \def\vhauthorname{Autor(es)}%
  \def\vhchangename{Descripción}%
}

% patch the macro that selects the captions.
\usepackage{etoolbox}
\apptocmd{\vh@setcaptions}{%
    \vh@Ifundefined{l@spanish}\else
      \ifnum \language=\l@spanish\relax
        \vh@SpanishCaptions
      \fi
    \fi
}{\typeout{vhistory patched succesfuly}}{\typeout{can't patch vhistory}}


\immediate\write18{sh ./vc -f}
\InputIfFileExists{vc}{}{}
\immediate\write18{./parselog.py > version_history.tex}

\tracingpatches
\apptocmd{\maketitle}{%
\clearpage
\InputIfFileExists{version_history.tex}{}{}
\newpage
}%
{\typeout{maketitle patched succesfully}}{\typeout{can't patch maketitle}}


