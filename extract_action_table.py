from openpyxl import load_workbook, Workbook
workbook = load_workbook(filename="Risk_Register.xlsx")
print(workbook.sheetnames)
sheet = workbook.active

risk_table = sheet["J8:O13"]

filename = 'actions.xlsx'
workbook = Workbook()
new_sheet = workbook.active

shift_rows_up = 0
for row in risk_table:
    # Don't copy rows with empty 'action' column by
    # shifting the target row up
    if row[3].value == None:
        shift_rows_up = shift_rows_up + 1
        continue
    for cell in row:
        new_location = cell.offset(-shift_rows_up, 0).coordinate
        # print(cell.coordinate, shift_rows_up, new_location)
        new_sheet[new_location].value = cell.value

workbook.save(filename=filename)
print("Finished extracting cells.")
