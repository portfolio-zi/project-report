\mode<article>
\chapter{\enquote{Visual analytics} (exploración/análisis descriptivo)}
\label{sec:visu-analyt-expl}
\mode*

% Notebook:
% • Tareas técnicas de análisis descriptivo de los datos mediante visual analytics,
% generando diferentes visualizaciones y caracterizaciones de diversas combinaciones de
% los datos, con el objetivo de extraer conclusiones que ayuden a extraer un valor
% intermedio de los datos y a entender mejor sus características para planificar de
% manera adecuada la siguiente fase de modelado con técnicas de machine learning.
% Contenidos para este apartado en la memoria:
% • Identificar los métodos descriptivos utilizados en este análisis exploratorio de los datos
% y argumentar su utilización (justificación argumentada).
% • Caracterizar con detalle los resultados obtenidos en las diferentes visualizaciones
% descriptivas y los ‘insights’ extraídos en cada caso como aporte de valor para la
% siguiente fase, de cara a establecer los modelos predictivos a utilizar.
% • Conclusiones acerca del aprendizaje sobre los datos que se ha obtenido en esta fase y
% el valor que aporta dentro del despliegue de un proyecto de analítica de datos
% (conectándolo con los objetivos de negocio que motivan el proyecto, así como a
% posibles beneficios adicionales a esa motivación inicial).

\section{Visualización de los datos}

Todos los datos en este análisis forman series temporales; cada conjunto de características tiene un \timestamp\ asociado. La forma más sencilla de visualizar los datos es representar cada carácteristica frente a su \timestamp.

Para ello se ha desarrollado un \dashboard\ utilizando el paquete de visualización \variable{bokeh} \cite{bokeh2020}. Para este \dashboard\ se ha creado una aplicación web con tres pestañas. En la primera pestaña se muestran los datos de la materia prima (mineral de entrada y solventes). En la segunda pestaña se representan los parámetros de operación de las siete columnas. La tercera pestaña muestra las características de calidad: porcentaje de hierro y sílice en el resultado final.

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{dashboard}
  \caption{Visualización de los parámetros de calidad en el \dashboard.}
  \label{fig:dashboard}
\end{figure}


\section{Análisis inicial}

\epigraph{(Los cálculos presentados en esta sección se encuentran en el notebook CRISP-DM\_04\_visualanalytics.ipynb)}{}

En primer lugar, es posible analizar visualmente la distribución de datos de cada característica. Para ello se ha generado un histograma de cada una de las columnas de datos. La \cref{fig:histos} muestra tres ejemplos. En un caso la distribución es cercana a una distribución normal (\subref{fig:strach_f}). Otras distribuciones muestran un comportamiento bimodal (\subref{fig:ore_f}) y otros parámetros operativos tienen \enquote{casi} valores discretos (\subref{fig:c6_air_f})\footnote{Estas gráficas se han generado en el notebook CRISP-DM\_03\_prepocess\_cleaning.ipynb}.  
No parece que se pueda obtener información más relevante (comparación de distintos tipos de distribuciones y sus parámetros poblacionales) de estas gráficas.


\begin{figure}
  \centering
  \begin{subfigure}[b]{0.3\textwidth}
        \includegraphics[width=\textwidth]{../data/figures/histstarch_f.pdf}
        \caption{Flujo de almidón}
        \label{fig:strach_f}
    \end{subfigure}\quad
  \begin{subfigure}[b]{0.3\textwidth}
        \includegraphics[width=\textwidth]{../data/figures/histore_f.pdf}
        \caption{Flujo de pulpa}
        \label{fig:ore_f}
    \end{subfigure}\quad
  \begin{subfigure}[b]{0.3\textwidth}
        \includegraphics[width=\textwidth]{../data/figures/histc6_air_f.pdf}
        \caption{Flujo aire columna 6}
        \label{fig:c6_air_f}
    \end{subfigure}
  \caption{Visualización de algunos histogramas de las características de los datos.}
  \label{fig:histos}
\end{figure}


\section{Estacionalidad}

No es esperable encontrar patrones de estacionalidad en un proceso industrial no repetitivo. Sin embargo se han realizado los cálculos para comprobarlo. Para ello, se ha analizado la proporción de hierro y sílice en la materia prima usando el paquete \variable{statsmodels} \cite{statsmodels}.

Este paquete permite descomponer una serie temporal en componentes de tendencia, estacionalidad y residuo. Se pueden seleccionar dos modelos naífs: aditivo o multiplicativo:
\begin{align*}
  \text{aditivo:}\quad&  Y_t = T_t + S_t + r_t\\
  \text{multiplicativo:}\quad& Y_t = T_t  S_t  r_t,
\end{align*}
donde $Y_t$ es el valor observado en el instante $t$, $T$ es la tendencia, $S$ es el valor de la estacionalidad para la frecuencia de la serie temporal y $r$ es el residuo. Como los datos de porcentaje de hierro y sílice se miden cada hora, primero se ha muestreado la serie temporal a esa frecuencia tomando la media. 

\begin{figure}
  \centering
  \begin{subfigure}[b]{\textwidth}
        \includegraphics[width=\textwidth]{../data/figures/seassonality/add_iron.pdf}
        \caption{Hierro con modelo aditivo.}
        \label{fig:seassonal_add}
      \end{subfigure}
      
  \begin{subfigure}[b]{\textwidth}
        \includegraphics[width=\textwidth]{../data/figures/seassonality/mul_silica.pdf}
        \caption{Sílice con modelo multiplicativo.}
        \label{fig:sesssonal_mul}
    \end{subfigure}
  \caption{Descomposición estacional del porcentaje de hierro y sílice en la materia prima.}
  \label{fig:estacionalidad}
\end{figure}

Como puede observarse en la \cref{fig:estacionalidad}, en los dos casos la \enquote{tendencia} sigue prácticamnte la serie observada y el valor de la estacionalidad es muy pequeño. Podemos concluir por lo tanto, que estas series temporales no muestran estacionalidad alguna.

\section{Autocorrelación}

Al analizar los coeficientes de autocorrelación del \silice\ resultante, se puede observar que con el conjunto de datos iniciales (frecuencia \SI{20}{\second}), las medidas están muy relacionadas la una con la otra (ver \cref{fig:autocorr_20s}). Esto es un resultado obvio, puesto que los datos de toda una hora tienen el mismo valor. Cierta correlación se mantiene para la serie temporal muestreada a \SI{1}{\hour} (\cref{fig:autocorr_1h}). La correlación desaparece para una frecuencia de un día (\cref{fig:autocorr_1d}). El límite de tener cierta correlación se puede ver cuando la serie temporal se muestrea a la duración de un turno laboral de 8 horas (\cref{fig:autocorr_8h}). 

\begin{figure}
  \centering
  \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{../data/figures/autocorr/20s_final_silica.pdf}
        \caption{\SI{20}{\second}}
        \label{fig:autocorr_20s}
    \end{subfigure}\quad
  \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{../data/figures/autocorr/1h_final_silica.pdf}
        \caption{\SI{1}{\hour}}
        \label{fig:autocorr_1h}
      \end{subfigure}

  \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{../data/figures/autocorr/1d_final_silica.pdf}
        \caption{\SI{1}{\day}}
        \label{fig:autocorr_1d}
    \end{subfigure}\quad
  \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{../data/figures/autocorr/8h_final_silica.pdf}
        \caption{\SI{8}{\hour}}
        \label{fig:autocorr_8h}
      \end{subfigure}
  \caption{Correlación del porcentaje de sílice final para distintas frecuencias de muestreo.}
  \label{fig:autocorr}
\end{figure}

\todo{crosscorrelation stadsmodel}

\mode<presentation>

\begin{frame}
  \frametitle{Analítica Visual}
  \begin{block}{Distribuciones}
    \begin{enumerate}
    \item Distintas características muestran distribuciones totalmente distintas (normales, bimodales, \ldots)
    \item Al ser parámetros de operación no parece que se obtenga información relevante
    \end{enumerate}
  \end{block}

  \begin{block}{Estacionalidad}
    \begin{itemize}
    \item Modelo aditivo y modelo multiplicativo
    \item No se observa estacionalidad de ningún tipo
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Autocorrelación}
  \only<1>{
  \begin{itemize}
  \item Se ha estudiado la correlación del \% sílice final
  \item Hay correlación en frecuencias \SI{20}{\second}, \SI{1}{\hour}; pero no \SI{1}{\day}
  \item Justo en el límite en algunos puntos para una frecuencia de turno de trabajo \SI{8}{\hour}
  \end{itemize}
  }
  \begin{center}
    \includegraphics<2>[width=\textwidth]{../data/figures/autocorr/20s_final_silica.pdf}
    
    \includegraphics<3>[width=\textwidth]{../data/figures/autocorr/1h_final_silica.pdf}
    
    \includegraphics<4>[width=\textwidth]{../data/figures/autocorr/1d_final_silica.pdf}
    
    \includegraphics<5>[width=\textwidth]{../data/figures/autocorr/8h_final_silica.pdf}
  \end{center}
\end{frame}


\begin{frame}
\frametitle{Dashboard}
\begin{center}
  \includegraphics[width=\linewidth]{figures/dashboard}
\end{center}
\end{frame}

\mode*

%%% Local Variables:
%%% TeX-parse-self: t
%%% TeX-auto-save: t
%%% TeX-master: "base.report"
%%% TeX-engine: luatex
%%% TeX-command-extra-options: "-shell-escape"
%%% ispell-local-dictionary: "castellano"
%%% End:

