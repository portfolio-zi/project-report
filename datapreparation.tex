\mode<article>
\chapter{Preprocesamiento y limpieza de datos}
\label{sec:prepr-y-limp}

\epigraph{(Los cálculos presentados en esta sección se encuentran en el notebook CRISP-DM\_03\_prepocess\_cleaning.ipynb)}{}
\mode*

% Notebook:
% • Tareas técnicas de transformación (preprocesamiento y limpieza) de los datos iniciales
% en bruto (una o varias fuentes a integrar), para configurar el dataset (con una o
% diferentes vistas de datos) sobre el que se aplicarán los siguientes pasos de analítica
% Contenidos para este apartado en la memoria:
% • Describir y caracterizar el conjunto de datos iniciales en bruto (fuente, formato,
% dimensión, variables) y el dataset obtenido tras esta fase de preprocesado y limpieza
% (una o varias vistas de datos, formato, dimensión, variables).
% • Argumentar la idoneidad de la selección de fuentes iniciales de datos y de variables
% concretas como input de inicio para el proyecto. Ligar este aspecto con la descripción
% de objetivos planteados en la planificación del proyecto (apartado 2).
% • Listar y comentar los problemas cualitativos y cuantitativos encontrados en el conjunto
% de datos (aflorar qué problemas nos encontramos en cuanto a la calidad e integración
% de datos para abordar el análisis con unas mínimas garantías).
% • Identificar los métodos de preprocesamiento y limpieza de datos utilizados y
% argumentar su utilización (justificación argumentada).
% • Conclusiones acerca de la necesidad e idoneidad de la tarea de preprocesado.

\section{Descripción del conjunto de datos}

El conjunto de datos se ha obtenido a través de los profesores del curso.  Son un subconjunto continuo de los datos presentados en un \enquote{desafío} presentado en la plataforma \emph{kaggle} \cite{dataset}. Los datos almacenados en formato \ac{csv} contienen 184364 registros de $25$ columnas; ocupando un total de \SI{35.2}{\mebi\byte}. 

El significado de cada columna de datos es el siguiente:
\begin{description}
\item[Columna 1] Identificador de número de registro. Es un número entero consecutivo y único. Puede utilizarse a modo de índice en la estructura de datos \dataframe, pero será sustituido por el \timestamp\ del registro.
\item[Columna 2] Fecha y hora del registro. La hora está dada hasta una resolución de minutos. Hay un registro nuevo cada \SI{20}{\second} y se ha calculado el \enquote{segundo} correspondiente a cada registro. Este \timestamp\ más completo ha sido utilizado como índice en los \dataframe.
\item[Columna 3] Porcentaje de hierro en el mineral de entrada.
\item[Columna 4] Porcentaje de sílice en el mineral de entrada.
\item[Columna 5] Flujo de almidón (\si{\metre\cubed\per\hour}).
\item[Columna 6] Flujo de amina (\si{\metre\cubed\per\hour}).
\item[Columna 7] Flujo de pulpa de mineral de entrada (\si{\tonne\per\hour}). 
\item[Columna 8] pH de la pulpa de mineral (escala de 0 a 14).
\item[Columna 9] Densidad de la pulpa de entrada (\si{\kilo\gram\per\centi\metre\cubed}).
\item[Columnas 10--16] Flujo de aire en las columnas de flotación (\si{\normalmetre\cubed\per\hour})\footnote{\si{\normalmetre\cubed} se refiere a \enquote{Normal metro cúbico,} volumen de gas en condiciones normales (\SI{0}{\degree} y presión a nivel de mar).}
\item[Columnas 17--22] Nivel de la espuma en la columna de flotación (\si{\milli\metre}).
\item[Columna 23] Porcentaje de hierro al finalizar el proceso de flotación (\SIrange{0}{100}{\percent}).
\item[Columna 24] Porcentaje de \silice\ al finalizar el proceso de flotación (\SIrange{0}{100}{\percent}).
\end{description}




\mode<presentation>

\begin{frame}
  \frametitle{Conjunto de datos iniciales}
  \begin{itemize}
  \item 184365 filas con 25 columnas en formato \textsl{CSV} (\SI{46}{\mega\byte})
  \item columna 1: identificador de registro (número entero consecutivo)
  \item 2: fecha y hora
  \item 3 y 4: porcentaje de hierro y sílice en el mineral de entrada 
  \item 5--9: Parámetros de operación más importantes (flujo de almidón, flujo de amina, flujo de la pulpa mineral de entrada, pH de la pulpa de mineral de entrada, densidad de la pulpa de mineral)
  \item 10--16: flujo de aire de las 7 columnas de flotación
  \item 17-23: nivel de espuma de las 7 columnas
  \item 24: porcentaje hierro en pulpa final
  \item 25: porcentaje sílice en pulpa final
  \end{itemize}
\end{frame}

\mode*

Los valores de los parámetros de operación (condiciones de la pulpa inicial y columnas de flotación) son recogidos cada \SI{20}{\second}. La calidad final del mineral (porcentaje de hierro y sílice) se mide en laboratorio. Esta medida requiere una hora de trabajo; por lo que los valores de las dos últimas columnas son constantes durante una hora; y tienen un desfase de una hora respecto a los parámetros de operación.


\mode<presentation>

\begin{frame}
  \frametitle{frecuencia de muestreo}
  \begin{block}{Medidas parámetros de operación}
    Estos parámetros son medidos con una frecuencia de \SI{20}{\second}.
  \end{block}
  \begin{block}{Medidas parámetros de calidad}
    Los parámetros de calidad
    \begin{itemize}
    \item Porcentaje de hierro final
    \item Porcentaje de \silice\ final
    \end{itemize}
    son medidas de laboratorio, que tardan \SI{1}{\hour}, y por lo tanto:
    \begin{itemize}
    \item \SI{1}{\hour} de frecuencia de medida
    \item \SI{1}{\hour} de retraso
    \end{itemize}

  \end{block}
\end{frame}
\mode*

Se ha comprobado que no hay valores nulos (\variable{nan}) ni registros duplicados. Hay 44 entradas con \timestamp\ \DTMdisplay{2017}{07}{29}{}{7}{00}{00}{}{}; y 1024 con 180 registros. Estos últimos representan  datos completos de 42 días y 16 horas, mientras que el primero son los últimos \SI{880}{\second} con ese \timestamp.  Los \timestamp\ representan exactamente eso 42 días y 16 horas. Por lo tanto, se puede concluir que no falta ninguna hora en el conjunto de datos.


Las columnas que representan porcentajes de materia (hierro y \silice\ en la materia prima y en el mineral producido) están en el rango de \SIrange{0}{100}{\percent}. De hecho, la suma de porcentaje de hierro y sílice al principio y al final del proceso está siempre por debajo de \SI{100}{\percent}.

Se han buscado valores atípicos, \outlier, extremos utilizando la técnica del \ac{iqr} \cite[pág. 44]{tuckey1977}. Los valores atípicos extremos son aquellos que están más allá de $3\text{IRQ}$. Se encuentran \outlier\ en cuatro columnas: flujo de aire en columnas 1, 2 y 3; y en el nivel de la columna 2. En total, hay \inenglish{outliers} en 54962 registros. No hay filas con más de 3 \inenglish{outliers}. Al no disponer de conocimiento experto sobre estos valores, se tomarán como correctos y no serán eliminados\footnote{El conjunto de datos obtenido de \inenglish{kaggle} ha sido trabajado por el autor del mismo, y podemos suponer que los valores erróneos (sensores que no funcionan, \ldots) han sido eliminados.}.

El análisis de las correlaciones entre las distintas características del conjunto de datos solo muestran una relación importante (mayor que 0.8) %
\todo{explicar paerson}%
para los pares de porcentaje de hierro/sílice en la materia prima y resultado final. Es un resultado esperable, puesto que una mayor concentración de uno significa una concentración menor del otro.

Dado que la medida de calidad que se quiere predecir está retrasada una hora, se ha añadido una columna (\variable{final\_silica\_p\_lag1}) con los datos de sílice desplazados esa ventana de tiempo. Para comprobar si se puede predecir el contenido de sílice con más tiempo de antelación; se ha creado otra columna con un desplazamiento de dos horas\footnote{No ha habido tiempo para intentar predecir esta variable.}.

Finalmente, se ha eliminado la columna del porcentaje de hierro en el mineral resultante. No es una característica que podamos usar para predecir la concentración de sílice.

\section{Reducción de características}
\label{sec:rfe}

Un aspecto que puede mejorar la creación de modelos predictivos es la reducción de características. En primer lugar se ha calculado la dispersión relativa. Las variables con menor valor, alrededor o menor a \SI{1}{\percent}, son el flujo de aire en las columnas 4 y~5. Estas dos variables podrían ser candidatas a no tener efecto en la variable a predecir.

Se ha llevado a cabo un proceso de \ac{rfe}. Se ha tomado como objetivo reducir el número de características a 15, utilizando un \ac{svr} con \inenglish{kernel} lineal. Las características seleccionadas son el flujo de amina y pulpa entrante, el pH y densidad de la pulpa, el flujo de aire de todos las columnas (menos la 6) y los niveles de todas las columnas (menos 2 y 6). Por lo tanto, este método contradice la intuición anterior de eliminar las variables con menor dispersión relativa.

No se ha optado por realizar un \ac{pca}, debido a la dificultad de interpretar el significado de los componentes obtenidos. Sería difícil implementar un sistema basado en esos componentes en una planta de operación.

\section{Conclusiones}

Se puede concluir que el conjunto de datos es de gran calidad, homogeneidad y fiabilidad en el sentido de obtener datos en todos los \timestamp\ de interés. La limpieza y transformación de los datos, necesaria para poder crear un modelo de regresión que pueda predecir la cantidad de sílice al final del proceso, es mínima.  


\mode<presentation>

\begin{frame}[allowframebreaks]
  \frametitle{Preprocesamiento y limpieza de datos}
  \begin{block}{Punto de partida}
    \begin{itemize}
    \item No datos nulos
    \item No datos vacíos
    \item No datos duplicados
    \end{itemize}
  \end{block}
  \begin{block}{Trabajo realizado}
    \begin{itemize}
    \item Convertir \emph{timestamp} \texttt{FECHA~HH:MM} a \texttt{FECHA~HH:MM:SS}
    \item Detección de \emph{outliers} (distancia intercuartil); eliminaría
      \SI{30}{\percent} de los datos
    \item Correlación únicamente entre \% \silice\ final y \% hierro final
    \end{itemize}
    \framebreak{}
  \end{block}
  \begin{block}{Reducción de características}
    \begin{itemize}
    \item Dispersión relativa 
    \item \ac{rfe} con estimador \ac{svr}
    \item Reducir las características a 15
    \item No se ha realizado PCA o similar
    \end{itemize}
  \end{block}
  \begin{block}{Conclusiones}
    \begin{itemize}
    \item Conjunto de datos \textquote[][]{limpio}
    \item Procesado requerido \textquote[][]{mínimo.}
    \end{itemize}
  \end{block}

\end{frame}
\mode*




%%% Local Variables:
%%% TeX-parse-self: t
%%% TeX-auto-save: t
%%% TeX-master: "base.report"
%%% TeX-engine: luatex
%%% TeX-command-extra-options: "-shell-escape"
%%% ispell-local-dictionary: "castellano"
%%% End:
