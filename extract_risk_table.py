from openpyxl import load_workbook, Workbook
workbook = load_workbook(filename="Risk_Register.xlsx")
print(workbook.sheetnames)
sheet = workbook.active

risk_table = sheet["B8:I13"]

filename = 'risks.xlsx'
workbook = Workbook()
new_sheet = workbook.active

for row in risk_table:
    for cell in row:
        new_sheet[cell.coordinate].value = cell.value

workbook.save(filename=filename)
print("Finished extracting cells.")
