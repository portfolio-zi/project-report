# TODO for CRISP-DM report template

  - Move `parselog.py` from python2 to python3.
  - Inplement `\code`, `\project`, ... macros in the class.
  - Allow to load packages in a separate file not to clutter the preamble.
  - Move font definitions for figures and presentation?
  - Move colors to sty file for figures?
